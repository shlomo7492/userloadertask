Users Manegement

1. The app loads 20 user records from the api that then render to the dom.
2. Each user data is displayed in separate row and includes the following fields: thumbnail profile picture, name, address and button 'X' (for removing)
3. Button X removes the row from the dom and the data.
4. Search form:
   4.1. If no data is entered in the search field the list of users stays unchanged.
   4.2. If the search word do not match any user or the filtering criteria is wrong error message is returned instead of the user list.
