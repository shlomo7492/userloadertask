var output = "";
var personArr = [];
var searchObject = {
  search: "",
  filter: "name"
};

// Get data from the api
const getUsers = async () => {
  const response = await axios(
    "https://randomuser.me/api/?nat=us,gb&results=20&inc=name,location,dob,picture"
  );
  return response.data.results;
};

//Fetching users data on initial loading of the page.
// Formating and populating data array for easy rendering
// in the browser
if (personArr.length === 0) {
  const myData = getUsers();
  personArr = Promise.all([myData]).then(res => {
    const data = [];
    res[0].map(el => {
      const name = `${el.name.first} ${el.name.last}`;
      const age = el.dob.age;
      const address = `${el.location.street}, ${el.location.city}`;
      const picture = el.picture.thumbnail;
      const person = { name, age, address, picture };
      data.push(person);
    });
    return data;
  });
}

//Loads users to the dom
const loadUsers = userList => {
  if (!userList) {
    userList = personArr;
  }
  let output = "";
  document.querySelector(".output").innerHTML =
    "<div class='loading'><h2>Loading...</h2></div>";
  userList.then(data => {
    if (data.length === 0) {
      output = `<h4> There are no data matching your criteria! 
          Please refine your search! </h4>`;
    }
    for (let i = 0; i < data.length; i++) {
      output =
        output +
        `<div class="row">
              <div class="thumbnail"><img src="${data[i].picture}"></div>
              <div class="text-data">Name:<br> ${data[i].name}</div>
              <div class="text-data">Address:<br> ${data[i].address}</div>
              <div class="text-data">Age:<br> ${data[i].age}</div>
              <div id=${i} class="text-data" onclick="updateView(${i})"><div class="close">x</div></div>
          </div>`;
    }
    document.querySelector(".output").innerHTML = output;
  });
};

//Updating the dom after clicking the X button
const updateView = index => {
  personArr = personArr.then(data => {
    data.splice(index, 1);
    return data;
  });
  loadUsers();
};

//Filtering and updating
const filterAndUpdateDom = () => {
  //taking the current state of searchObject
  //Create separate promise object based on the search/filter criteria
  const userList = searchObject.search
    ? personArr.then(data => {
        const filteredUsers = [];
        data.map(user => {
          switch (searchObject.filter) {
            case "name":
              {
                if (user.name === searchObject.search) {
                  filteredUsers.push(user);
                }
              }
              break;
            case "address":
              {
                if (user.address === searchObject.search) {
                  filteredUsers.push(user);
                }
              }
              break;
            case "age":
              {
                if (parseInt(user.age) === parseInt(searchObject.search)) {
                  filteredUsers.push(user);
                }
              }
              break;
            default:
              break;
          }
        });
        return filteredUsers;
      })
    : personArr;

  //Call loadUsers(userList) where userList is a
  //promise containing filtered user list
  loadUsers(userList);
};

//Event listeners for the Search form

document.querySelector(".filter-form").addEventListener("submit", e => {
  //Preventing the default behaviour of the form on submission
  e.preventDefault();
  searchObject.search = e.target.elements[0].value;
  //Call to filterAndUpdateDom function
  filterAndUpdateDom();
});

document.querySelector(".filter-form").addEventListener("change", e => {
  if (e.target.name === "filter") {
    searchObject.filter = e.target.value;
    if (searchObject.search !== "") {
      //Call to filterAndUpdateDom function
      filterAndUpdateDom();
    }
  } else if (e.target.name === "usersearch") {
    searchObject.search = e.target.value;
    //Call to filterAndUpdateDom function
    filterAndUpdateDom();
  }
});
